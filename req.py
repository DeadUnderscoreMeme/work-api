from suds.client import Client

url = "./addmatch.wsdl"

client = Client('file:///mnt/sda1/Work/API%20Scripting/addmatch.wsdl')

AvailableAddressesRequest = client.factory.create('AvailableAddressesRequest')

UserCredentials = client.factory.create('ns2:Credentials')

Address = client.factory.create('Address')

UserCredentials.AgentID = 1612
UserCredentials.Password = 'MaV3r1Ckl1V3aP1'
UserCredentials.Username = 'api@maverick.co.uk'

Address.PostCode = 'OL9 9JN'

AvailableAddressesRequest.UserCredentials = [UserCredentials]
AvailableAddressesRequest.Address = [Address]

# print(AvailableAddressesRequest)
# print(UserCredentials)
# print(Address)

print(client.factory.create('AvailableAddressesResponse'))

a = client.service.GetAvailableAddresses(AvailableAddressesRequest)
print(a)
