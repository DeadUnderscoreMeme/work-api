import os
import sys
import json
import urllib
import psycopg2
from suds.client import Client
from suds.sudsobject import asdict

def get_postcode():

    conn = psycopg2.connect(database="OpticDB", 
    user="postgres", password="Reeve123", 
    host="optic-database.codontximo4e.ap-south-1.rds.amazonaws.com", 
    port="5432")
    curs = conn.cursor()

    curs.execute('select PostCode from remainingpostcodes where fetched = false limit 1')
    fetch = curs.fetchall()
    postCode = fetch[0][0]

    curs.execute("update remainingpostcodes set fetched = true where PostCode = '{}'".format(postCode))
    
    conn.commit()
    curs.close()
    conn.close()
    # print(postCode)
    return postCode

def send_request(postCode):

    client = Client('file:///mnt/sda1/Work/API%20Scripting/addmatch.wsdl')

    AvailableAddressesRequest = client.factory.create('AvailableAddressesRequest')

    UserCredentials = client.factory.create('ns2:Credentials')

    Address = client.factory.create('Address')

    UserCredentials.AgentID = 1612
    UserCredentials.Password = 'MaV3r1Ckl1V3aP1'
    UserCredentials.Username = 'api@maverick.co.uk'

    Address.PostCode = postCode

    AvailableAddressesRequest.UserCredentials = [UserCredentials]
    AvailableAddressesRequest.Address = [Address]

    suds_object = client.service.GetAvailableAddresses(AvailableAddressesRequest)

    return suds_object

def sudsObj_to_dict(sudsObj, postCode):

    try:
        sites_sudsObj = sudsObj.Sites

        sites_pyDict = []

        for site in sites_sudsObj.PostCodeValidatedSite:
            site_pyDict = Client.dict(site.Address)
            del site_pyDict['Technologies']
            sites_pyDict.append(site_pyDict)

        return sites_pyDict

    except AttributeError:
        
        with open ('error_postcodes','a') as file:
            file.write(postCode)
            file.write(',')
            file.write('\n')

        return False     

def cleanedReqs(pyDict):

    cleanedRequests = []

    for dictElement in pyDict:
        
        request_element = 'insert into scrapingphaseone ('
        
        for key in dictElement.keys():
            request_element+=key
            request_element+=', '
        request_element = request_element[:-2]
        request_element+=') values ('

        for value in dictElement.values():
            value2 = format_value(value)
            request_element+=str(value2)
            request_element+=','
        request_element = request_element[:-1]
        request_element+=');'

        cleanedRequests.append(request_element)

    return cleanedRequests

def format_value(val):
    
    if str(type(val)) == "<class 'suds.sax.text.Text'>":
        val = str(val)
        val = val.replace("'","")
        val = "'"+val+"'"

    return val

def append_to_table(responseDict,postCode):

    conn = psycopg2.connect(database="OpticDB", 
    user="postgres", password="Reeve123", 
    host="optic-database.codontximo4e.ap-south-1.rds.amazonaws.com", 
    port="5432")
    curs = conn.cursor()

    for response in responseDict:
        curs.execute(response)

    curs.execute("update remainingpostcodes set scraped = true where postcode = '{}';".format(postCode))

    conn.commit()
    curs.close()
    conn.close()    

def main():

    path = urllib.parse.urljoin('file:', urllib.request.pathname2url(os.path.abspath("./addmatch.wsdl")))
    client = Client('file:///mnt/sda1/Work/apiscript/addmatch.wsdl')

    while(True):
        try:
            postCode = get_postcode()
            print('Postcode is : ', postCode)
            
            responseSudsObj = send_request(postCode)
            responseDict = sudsObj_to_dict(responseSudsObj, postCode)

            if(responseDict == False):
                continue

            print('Data Gotten with {} results'.format(len(responseDict)))

            cleaned = cleanedReqs(responseDict)
            
            append_to_table(cleaned,postCode)
            print('Appended {} Addresses for the Postcode {}'.format(len(responseDict), postCode))

        except:
            pass
if __name__ == '__main__':
    main()